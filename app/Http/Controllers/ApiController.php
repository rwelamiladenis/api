<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ApiController extends Controller
{
  public $base_uri;

  function __construct() {
    $this->base_uri = '';
    // $this->base_uri = 'https://192.168.1.105/openmrs/ws/rest/v1/';
    // $request = new Request('PUT', 'http://httpbin.org/put');
  }

  public function test() {
    $client = new Client();
    $request = new Request('GET', 'http://daas.tujengetech.co.tz/api/forums');
    $response = $client->send($request);
    return $response->getBody();
  }

  public function postPayment() {
    $client = new Client(['curl' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0]]);
    $headers = ['Content-Type' => 'text/xml; charset=UTF8'];
    $body = "<gepgPmtSpInfo><PymtTrxInf><TrxId></TrxId><SpCode></SpCode><PayRefId></PayRefId><BillId>SO1538669669068</BillId><PayCtrNum>9090</PayCtrNum><BillAmt>2000</BillAmt><PaidAmt>2000</PaidAmt><BillPayOpt></BillPayOpt><CCy></CCy><TrxDtTm></TrxDtTm><UsdPayChnl></UsdPayChnl><PyrCellNum>123456789</PyrCellNum><PyrName></PyrName><PyrEmail></PyrEmail><PspReceiptNumber></PspReceiptNumber><PspName></PspName><ctrAccNum></ctrAccNum></PymtTrxInf></gepgPmtSpInfo>";


    $request = new Request('POST', 'https://192.168.1.105/openmrs/ws/rest/v1/receiveResponse/post_payment_information', $headers, $body);
    $response = $client->send($request);
    return $response->getBody();
  }

  public function getControlNumber() {
    $client = new Client(['curl' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0]]);
    $headers = ['Content-Type' => 'text/xml; charset=UTF8'];
    $body = '<gepgBillSubResp><BillTrxInf><BillId>SO1538813032108</BillId><TrxSts>GS</TrxSts><PayCntrNum>safas</PayCntrNum><TrxStsCode>7242;7627</TrxStsCode></BillTrxInf></gepgBillSubResp>';
    $request = new Request('POST', 'https://192.168.1.105/openmrs/ws/rest/v1/receiveResponse/get_control_number', $headers, $body);
    $response = $client->send($request);
    return $response->getBody();
  }

  public function getReconciliationResponse() {
    $client = new Client(['curl' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0]]);
    $headers = [
      'Content-Type' => 'application/xml; charset=UTF8',
      'Gepg-Com' => 'default.sp.in',
      'Gepg-Code' => 'SP222'
    ];

    $body = "
    <gepgSpReconcResp>
      <ReconcBatchInfo>
          <SpReconcReqId>1379794698</SpReconcReqId>
          <SpCode>SP222</SpCode>
          <SpName>GOV Agency</SpName>
          <ReconcStsCode>GOV Agency</ReconcStsCode>
        </ReconcBatchInfo>
        <ReconcTrans>
        <ReconcTrxInf>
          <SpBillId>FH4671150752</SpBillId>
          <BillCtrNum>991080222529</BillCtrNum>
          <pspTrxId>E991080222529</pspTrxId>
          <PaidAmt>3500</PaidAmt>
          <CCy>TZS</CCy>
          <PayRefId>9910222529</PayRefId>
          <TrxDtTm>2017-10-09T07:35:56</TrxDtTm>
          <CtrAccNum>0150211612834</CtrAccNum>
          <UsdPayChnl>FAHARI HUDUMA</UsdPayChnl>
          <PspName>NMB</PspName>
          <PspCode>PSP006</PspCode>
          <DptCellNum/>
          <DptName/>
          <DptEmailAddr/>
          <Remarks>Failed at PSP</Remarks>
          <ReconcRvs1></ReconcRvs1>
          <ReconcRsv2></ReconcRsv2>
         <ReconcRvs3></ReconcRvs3>
        </ReconcTrxInf>
      </ReconcTrans>
    </gepgSpReconcResp>";
    $request = new Request('POST', 'https://192.168.1.105/openmrs/ws/rest/v1/receiveResponse/get_reconciliation_response', $headers, $body);
    $response = $client->send($request);
    return $response->getBody();
  }

  public function cancelBillRequest() {
    $client = new Client(['curl' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0]]);
    $headers = ['Content-Type' => 'application/json; charset=UTF8'];
    $body = json_encode(['BillId' => 'SO1538669669068']);
    $request = new Request('POST', 'https://192.168.1.105/openmrs/ws/rest/v1/receiveResponse/cancel_bill_request', $headers, $body);
    $response = $client->send($request);
    return $response->getBody();
  }

  public function getSignRequest() {
    $client = new Client(['curl' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => 0]]);
    $headers = [
      'Content-Type' => 'application/xml; charset=UTF8',
      'Gepg-Com' => 'default.sp.in',
      'Gepg-Code' => 'SP111'
    ];
    $billId = round(microtime(true) * 1000);
    $signature = base64_encode("MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCceMdX/dn16mVe
CMQJwEBVpHh2YkcIrA06F7Aw0/2rlaq6SqV/oZtLXYD6K3E1vdy9b0mJ 1XeRgLe3
soJ5l1KrOg1lGJ0IopMcA6ANKRoxh59fp1fwoPI4p7e9i/TdSZbpf0254WvtxIQv
umtBRsaahXYUycAu91b3S0bGX9p0yFAEwLNRiI7KoW+okAZrkpiESlnbeqG0fqkp
jZFtLDEAYWqCi64MwiqAClyWYVOsFAIwfNWRdvRS/paDy8i21UQYduFyccCamzcl
SDagJnnfngktyStHUwBWQmL3+nJTyecsi2tGo9VGGnUieJf4ylDPaUIpQ0zCwhJ3
oWhM9elRAgMBAAECggEBAIZrp+0qsk114UFoTGmIFHy+f5JMGXQUwoQuh3lbFOiL
+hByQj0kK1IScNP58BbYFcNT0Jwtm7uPFcUlALvOSQG6LHm2LGmXqMN/RccokU0N
ktfM3lplGdGcG9glXOWh65iKec5/HoYztc9+2ddJIyLqFqgDLivq5nYp4BJZlEWP
3Eh08K76kvfh34ZPfYB9STKBdwImlzw06B+djJMsJ7h272o3fLBh9D7IO50I81dM
6c6nOsPYZnMTDJnihx+qH5onjCmLpRabWHZkgBrKXk3Ff2i4/RNwFpv4eEYtNVoJ
Q9puiW3L/tsL0l6aBTId67IxFWdP30kFpK6TtJM63kECgYEAzqbjDxM4Eo4tpWLu
HEs9mLYRIpNbNnin2v1VM0ozsA8p6LwK6VPwflrHrgwJ0srS2AuvR8qcbyNO1VvJ
DDlPHfMLepCjcX0zPGguVv1tyJeRyoZtRWg/FDhXn8KMU9ubw3VCQBkY8UblOGKu
wMez/CX9SAgBvs+IGGNS7r6IuxkCgYEAwdZDtQpVAVe1KjVEjoABlOSmmbr2mnWu
QPoum+ihKTuJ9msp3eiHFK6YdXz1HAR4Rgz0Lbd9EuHFiTuxqwGbvc9JBnQiJxKs
XaexqwC/GUnivlmx0ZlXBHmNWRxjP9M2coaXIJ5HVUH1jzPMbJiYJxK8T4Ca7D77
8r1Op/DDHvkCgYBwlDxIfyjh+vDlE7MyWM8SvcUx9+MOFscjJzXePkNQW8JyFtc+
68qtm1kC+W30lygsKJlx2/BDH5IX6+voK+RtJXYQbHGOJy3db8gm/eNsDx6OnZMz
56nNozAhScRW/uGpptzTvJh5AS1o/WHEhkcIS83jCvGgeMDkwF9yzvwAKQKBgQCr
a7e5FYgTheNbjnwwoZfEe2myrpuEAeq24An0K4jLFDJwUFlYPFmpurZtwLJrsrBD
ExWmkIz18+Bj+vymxOdct0JXiMDjWSpCMRl4p1JZJPR3bo9JU5iXBoAwfGBxg3K1
Wb2xRQjEp25o1GBG8bQtae5LSOq5XwIlq7Ex6ERG0QKBgHAii3fS3piHL8fD/7HE
biElfF8yNMphLzKeKMtln3Sn//0CB1O/ZN0ETSYKkzkqyxzVT7H7x+16I6La9pkS
KQvI6V5b60OqNv0PqJibKEEvYlJvzgH0+UgyWr3TGOY3w0ap38UZq8y9jBUnLbUO
DF2Xzifb9OmGn3pKmHFTy0UI");
    $body = "<Gepg><gepgBillSubReq>
    <BillHdr>
        <SpCode>SP111</SpCode>
        <RtrRespFlg>true</RtrRespFlg>
    </BillHdr>
    <BillTrxInf>
        <BillId>" . $billId . "</BillId>
        <SubSpCode>7001</SubSpCode>
        <SpSysId>HESLB001</SpSysId>
        <BillAmt>76937.39</BillAmt>
        <MiscAmt>0</MiscAmt>
        <BillExprDt>2019-06-30T00:00:00</BillExprDt>
        <PyrId>201900157221</PyrId>
        <PyrName>Paul</PyrName>
        <BillDesc>Property Rate</BillDesc>
        <BillGenDt>2017-12-18T00:00:00</BillGenDt>
        <BillGenBy>TCAA Admin</BillGenBy>
        <BillApprBy>TCAA Admin</BillApprBy>
        <PyrCellNum>255784785548</PyrCellNum>
        <PyrEmail/>
        <Ccy>TZS</Ccy>
        <BillEqvAmt>76937.39</BillEqvAmt>
        <RemFlag>false</RemFlag>
        <BillPayOpt>1</BillPayOpt>
        <BillItems>
            <BillItem>
                <BillItemRef>310001483660</BillItemRef>
                <UseItemRefOnPay>N</UseItemRefOnPay>
                <BillItemAmt>76937.39</BillItemAmt>
                <BillItemEqvAmt>76937.39</BillItemEqvAmt>
                <BillItemMiscAmt>0</BillItemMiscAmt>
                <GfsCode>1403707</GfsCode>
            </BillItem>
        </BillItems>
    </BillTrxInf>
</gepgBillSubReq>
  <gepgSignature>$signature</gepgSignature></Gepg>";
    $request = new Request('POST', 'http://154.118.230.18:80/api/bill/sigqrequest', $headers, $body);
    $response = $client->send($request);
    return $response->getBody();
  }
}
