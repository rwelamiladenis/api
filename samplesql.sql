select 
'1.1 Cumulative number of clients ever enrolled in care at this facility at beginning of the reporting quarter' AS "indicator",
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM (
SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	(crp.ctc_arv_status='CTC - ARV Status, Start ARV')
	AND  pvd.visit_Start_Date < '2018-11-10') AS tperiod
UNION

select 
'1.2 Number of clients NEWLY enrolled in HIV care at this facility during the reporting quarter (EXCLUDE TI)'  AS "indicator",
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM 
(SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
(crp.ctc_arv_status='CTC - ARV Status, Start ARV' )
AND pvd.visit_Start_Date >= '2018-09-10' AND 	pvd.visit_Start_Date <= '2018-11-10')
AS tperiod

UNION
SELECT '1.3 Cumulative number of clients ever enrolled in care at this facility at the end of the reporting quarter' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM
(SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	(crp.ctc_arv_status='CTC - ARV Status, Start ARV')
	AND  	pvd.visit_Start_Date <= '2018-11-10') as tperiod
	
UNION

SELECT '1.4 Number of clients who received care during the reporting quarter' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM

(SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE  pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <= '2018-11-10' )
	AS tperiod
	
UNION

SELECT '1.5 Number of clients who received CPT during the reporting quarter (subset of receieved care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM

    (SELECT  DISTINCT 
    "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM   ctc2_patient_record_form_poc crp
    INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN medication_data_default mdd ON mdd.patient_id = pvd.patient_id
	WHERE 
	(mdd.coded_drug_name like ('%CO-TRI%') OR mdd.coded_drug_name like ('%cotri%'))
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10'
	) as tperiod

	UNION

SELECT '1.5 Number of clients who received CPT during the reporting quarter (subset of receieved care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM

    (SELECT  DISTINCT 
    "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM   ctc2_patient_record_form_poc crp
    INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN medication_data_default mdd ON mdd.patient_id = pvd.patient_id
	WHERE 
	(mdd.coded_drug_name like ('%CO-TRI%') OR mdd.coded_drug_name like ('%cotri%'))
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10'
	) as tperiod
	
UNION

SELECT '1.6 Number of clients screened for TB in the reporting quarter (subset of received care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM
(SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.Ctc_Tb_Screening_And_Dx <> 'CTC - TB not Screened' and crp.Ctc_Tb_Screening_And_Dx <> '' 
	and crp.Ctc_Tb_Screening_And_Dx is not null
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10') as tperiod
	
UNION

SELECT '1.11 Number of clients who started on Isoniazid Preventive Therapy (IPT) during the reporting quarter (Pre ART and ART)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM

(SELECT   DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN medication_data_default mdd ON mdd.patient_id = pvd.patient_id
	
	WHERE 
	 mdd.coded_drug_name like ('%isonia%')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10')
	as tperiod
	
UNION

SELECT '1.7 Number of clients who started on TB treatment (Pre ART and ART) (subset of received care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM (select DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_TB_Rx <> 'CTC - Stop TB - Stopped'
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10')
	as tperiod
	
UNION

SELECT '1.8 Number of HIV positive clinically malnourished persons identified during the quarter (subset of received care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM(
    SELECT DISTINCT 
    "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
        crp.CTC_Nutritional_Status in ( 'CTC - Nutritional Status, SEV - Severely Malnourished','CTC - Nutritional Status, MOD - Moderate Malnourished')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10') as tperiod
	
UNION

SELECT '1.8 Number of HIV positive clinically malnourished persons identified during the quarter (subset of received care)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"

FROM(
    SELECT DISTINCT 
    "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
        crp.CTC_Nutritional_Status in ( 'CTC - Nutritional Status, SEV - Severely Malnourished','CTC - Nutritional Status, MOD - Moderate Malnourished')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10') as tperiod

UNION

SELECT '1.9 Number of HIV positive clinically malnourished received therapeutic and/or supplementary food (subset of malnourished)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from

(SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_Nutritional_Status in ( 'CTC - Nutritional Status, SEV - Severely Malnourished','CTC - Nutritional Status, MOD - Moderate Malnourished')
	and crp.CTC_Nutritional_Supplement in ( 'CTC - SF - Supplemental Food','CTC - TF - Therapeutic Food')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10') as tperiod
	
UNION

SELECT '1.13 Number of clients enrolled into care and not started ARV' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (  SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
    FROM ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_ARV_Status in ('CTC - ARV Status, Not started ARV')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10'
) as tperiod
union

SELECT '2.1 Cumulative number of persons ever started ART at this facility at beginning of the reporting quarter' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_ARV_Status  in ( 'CTC - ARV Status, Start ARV')
	AND
	 pvd.visit_Start_Date <= '2018-11-10'
) as tperiod

UNION

SELECT '2.2 Persons newly initiated on ART at this facility during the reporting quarter (EXCLUDE TI on ART)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_ARV_Status  in ( 'CTC - ARV Status, Start ARV')
	AND pvd.visit_Start_Date >= '2018-07-10' AND pvd.visit_Start_Date <=  '2018-11-10'
) as tperiod

UNION

SELECT '2.3 Cumulative number of persons ever started on ART at this facility at the end of the reporting quarter' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_ARV_Status  in ( 'CTC - ARV Status, Start ARV')
	AND  pvd.visit_Start_Date <=  '2018-11-10'
) as tperiod

UNION

SELECT '2.10 Number of persons on ART and already enrolled in the program who transferred into facility during the reporting period' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.CTC_ARV_Status  NOT IN ('CTC - ARV Status, Not started ARV','CTC - ARV Status, Stop')
	AND pvd.visit_Start_Date <= '2018-11-09' and "ID_Number" NOT LIKE '01-03-0200%'
) as tperiod

UNION

SELECT '2.11 Cumulative number of persons ever on ART at this facility at the end of the quarter (include TI on ART)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	(crp.ctc_arv_status NOT IN ('CTC - ARV Status, Not started ARV','CTC - ARV Status, Stop'))
	AND  pvd.visit_Start_Date <= '2018-11-09' and "ID_Number" LIKE '01-03-0200%') as tperiod
	
UNION

SELECT '2.4 Number of persons who are on 1st line regimen during the reporting period (subset of item 2.8)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT  DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	WHERE 
	(crp.ctc_arv_status  IN ('CTC - ARV Status, Start ARV','CTC - ARV Status, Continue ARV','CTC - ARV Status, Restart'))
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod

UNION

SELECT '2.5 Number of persons who are on 2nd line regimen during the reporting period (subset of item 2.8)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	(crp.ctc_arv_status  IN ('Switch to 2nd line'))
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
	
UNION

SELECT '2.15 Number of Clients who are on 3rd line regimen during the reporting period (INCLUDE TI ON ART)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.ctc_arv_status  IN ('Switch to 3rd line')
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
	
UNION

SELECT '2.8 Current number of clients on ART at the end of the quarter(Total of 1st,2nd and 3rd line regimen)' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	
	WHERE 
	crp.ctc_arv_status NOT IN ('CTC - ARV Status, Not started ARV','CTC - ARV Status, Stop')
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
	
UNION

SELECT '2.12 Total number of VL tests < 1000 copies/mL during reporting period' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, crp.obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN  lab_results lb ON lb.patient_id = pvd.patient_id
	
	WHERE 
	    lb.value_text::integer < 1000 and lb.test_name = 'CTC Viral Load' and lb.value_text not in ('DONE','')
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
	
UNION

SELECT '2.12 Total number of VL tests < 50 copies/mL during reporting period" as indicator' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, crp.obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN  lab_results lb ON lb.patient_id = pvd.patient_id
	
	WHERE 
	    lb.value_text::integer < 50 and lb.test_name = 'CTC Viral Load' and lb.value_text not in ('DONE','')
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
	
UNION

SELECT '2.14 Total number of VL test ≥ 1000 copies/mL during reporting period." as indicator' as "indicator", 
SUM(CASE WHEN tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'M' THEN 1 ELSE 0 END) AS "M,>=50 years",
SUM(CASE WHEN tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,Total",
SUM( CASE WHEN tperiod.AgeToDate::integer  < 1 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,<1 year",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 1 AND tperiod.AgeToDate::integer < 5 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,1-4 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 5 AND tperiod.AgeToDate::integer < 10 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,5-9 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 10 AND tperiod.AgeToDate::integer < 15 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,10-14 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 15 AND tperiod.AgeToDate::integer < 20 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,15-19 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 20 AND tperiod.AgeToDate::integer < 25 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,20-24 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 25 AND tperiod.AgeToDate::integer < 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,25-49 years",
SUM( CASE WHEN tperiod.AgeToDate::integer >= 50 AND tperiod.Gender = 'F' THEN 1 ELSE 0 END) AS "F,>=50 years"
from
    (SELECT DISTINCT 
     "ID_Number" as CTCID,pdd.age as AgeToDate, pdd.gender as Gender, crp.obs_datetime as visitStartDate	
FROM    ctc2_patient_record_form_poc crp
	INNER JOIN person_details_default pdd ON pdd.person_id = crp.patient_id
	INNER JOIN patient_program_data_default ppdd ON ppdd.patient_id = crp.patient_id and ppdd.program_id=1
	INNER JOIN program_attributes pa ON pa.patient_program_id = ppdd.patient_program_id
	INNER JOIN  patient_visit_details_default pvd ON crp.patient_id = pvd.patient_id
	INNER JOIN  lab_results lb ON lb.patient_id = pvd.patient_id
	
	WHERE 
	    lb.value_text::integer >= 1000 and lb.test_name = 'CTC Viral Load' and lb.value_text not in ('DONE','')
	AND pvd.visit_Start_Date >= '2018-07-02' AND pvd.visit_Start_Date <= '2018-11-02') as tperiod
